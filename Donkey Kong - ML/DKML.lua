--Lua script to interface with Donkey Kong NES ROM via Bizhawk emulator

if gameinfo.getromname() == "Donkey Kong Classics" then
	savestate.load("classicsTestState.State")
	ButtonNames = {
		"A",
		"B",
		"Up",
		"Down",
		"Left",
		"Right",
	}
	
	marioX = memory.read_u8(0x0046)
	marioY = memory.read_u8(0x0047)
	
	controller = {}
	--[[controller["P1 Right"] = true
	
	while (memory.read_u8(0x0046) < 200) do	
		joypad.set(controller)
		emu.frameadvance()
		--console.writeline(marioX)
	end
	
	controller["P1 Right"] = false
	controller["P1 Up"] = true
	
	while (memory.read_u8(0x0047) ~= 204) do	
		joypad.set(controller)
		emu.frameadvance()
		console.writeline(marioY)
	end
	
	
	
	controller["P1 Left"] = true
	controller["P1 Up"] = false
	
	while true do	
		joypad.set(controller)
		emu.frameadvance()
		--console.writeline(marioY)
	end]]--
	
	while true do
		if (memory.read_u8(0x0046) < 200) then
			controller["P1 Right"] = true
			joypad.set(controller)
			emu.frameadvance()
		
		elseif (memory.read_u8(0x0047) ~= 204) then
			controller["P1 Right"] = false
			controller["P1 Up"] = true
			joypad.set(controller)
			emu.frameadvance()
			
		else
			controller["P1 Left"] = true
			controller["P1 Up"] = false
			controller["P1 Right"] = false
			joypad.set(controller)
			emu.frameadvance()
		end
			
	end

else
	console.writeline("Donkey Kong file not found")
	
end