--For more info on the algorithm used, please read https://towardsdatascience.com/simple-reinforcement-learning-q-learning-fcddc4b6fe56 --

-------------LOGISTICAL VARIABLES: DO NOT EDIT!!!!---------------

Filename = "DK-1.state" --Save state
Buttons = { --Logical representation of button presses
	"Up", --1
	"Down", --2 --Design question: Should we remove?
	"Left", --3
	"Right", --4
	"Up-Left", --5
	"Up-Right", --6
	"A" --7 --Design question: Should we remove?
}
marioX = 0 --Dont think we're using this anywhere
marioY = 0 --Or this
marioOldX = 0 --Stores the x position of Mario at the beginning of the timestep
marioOldY = 0 --Stores the y position of Mario at the beginning of the timestep
EnemyXPositions = {} --Stores enemy x positions. Being used to freeze the enemies
EnemyYPositions = {} --Stores enemy y positions. Being used to freeze the enemies.
QTable = {} --Ya main boi
controller = {} --Controller obj used to set the joypad status
frameCounter = 0 --Counts frames
timeoutFrameCounter = 0 --Counts frames for timeout
button = "" --Stores a string of the button being pressed
exploreOrExploit = 0 --Container for epsilon checks
reward = 0 --Container variable for reward. NOTE: Currently using 255 - yPos as reward
currentState = 0 --Not being used, too scared to remove so don't do it
currentAction = 0 --Not used
newState = 0 --Not used
newAction = 0 --Not used

-------------DESIGN VARIABLES: MODIFY THESE!!!!---------------

epsilon = 0.4 --Exploration probability, i.e probability that Mario will pick a random action, rather than an informed one via querying the Q Table
learningRate = 0.9 --How much you accept the new values vs. the old ones. Should probably be as high as possible
gamma = 0.99 --Very important!!! Determines how important long-term rewards are to his decisions. Should be as high as possible but cannot be 1.
fixedFramesPerAction = 30 --How long to wait before evaluating - i.e, how many frames to hold a button before recording its reward and adding it to QTable
timeoutFrames = 500 --When Mario is stuck/isn't moving, how many frames to wait before resetting


function GetMarioX()
	return memory.read_u8(0x0046)
end

function GetMarioY()
	return memory.read_u8(0x0047)
end

function GetMarioStatus()
	return memory.read_u8(0x0096)
end

function FreezeEnemyPositions()
	local xMem = 0x0213
	local yMem = 0x0214
	for i=1, 12 do
		--Use readbyte because we only read the lower half for position values
		memory.writebyte(xMem, 0x00FF)
		memory.writebyte(yMem, 0x00FF)
		xMem = xMem + 0x0010
		yMem = yMem + 0x0010
	end
end

--Takes Mario's x and y position and converts it into a one-dimensional index into the QTable that represents the state
function GetIndexFromPosition(x,y)
	return (((x-1) * 255 + (y-1)) + 1)
end
--Takes a one-dimensional index into the QTable that represents the state and converts it back to Mario's X position
function GetXPositionFromIndex(i)
	return ((math.floor((i-1)/255)) + 1)
end
--Takes a one-dimensional index into the QTable that represents the state and converts it back to Mario's Y position
function GetYPositionFromIndex(i)
	return (((i-1)%255) + 1)
end

--Init qtable values of 255*255*7 to 0
function InitQTable()
	local stateSize = 255 * 255
	for i = 1, stateSize do
		QTable[i] = {}
		for j = 1, 7  do
			QTable[i][j] = 0
		end
	end
end

--Randomly decides (based on epsilon) whether to take a random action or to take an informed action by looking up the QTable
function TakeAction(currentState)
	exploreOrExploit = math.random() --Explore or exploit?  Using a global variable because I want to display the text in the main loop
	local actionTaken = 0
	if(exploreOrExploit <= epsilon) then --Choose exploration
		actionTaken = math.random(1,7) --Randomly selects an action
	else -- Choose exploitation
		actionTaken = GetBestAction(currentState) --Queries the QTable for the best action based of the current state
	end

	--Presses the button represented by the action
	button = Buttons[actionTaken]
	PressButton(button)

	return actionTaken --returns the action/button index
end 

--Evaluates maximum q value of actions for a given state index
function GetBestAction(currentState)
	local bestActionIndex = 1
	for actionIndex = 1, 7 do
		if QTable[currentState][actionIndex] > bestActionIndex then
			bestActionIndex = actionIndex
		end
	end
	return bestActionIndex
end

--Given a button from Buttons array, presses the corresponding button on the joypad
function PressButton(button)
	if button == "Up" then
		controller["P1 Up"] = true
	elseif button == "Down" then
		controller["P1 Down"] = true
	elseif button == "Left" then
		controller["P1 Left"] = true
	elseif button == "Right" then
		controller["P1 Right"] = true
	elseif button == "Up-Left" then
		controller["P1 A"] = true
		controller["P1 Left"] = true
	elseif button == "Up-Right" then
		controller["P1 A"] = true
		controller["P1 Right"] = true
	elseif button == "A" then
		controller["P1 A"] = true
	end
	joypad.set(controller)
end

--Unpresses all buttons
function ClearButtons()
	controller["P1 Up"] = false
	controller["P1 Down"] = false
	controller["P1 Left"] = false
	controller["P1 Right"] = false
	controller["P1 A"] = false
	joypad.set(controller)
end

function saveTable()
	
	local file = assert(io.open("table.txt", "w"))
	
	for i = 1, 255*255 do
		for j = 1, 7 do
			file:write(QTable[i][j], " ")
		end
		file:write("\n")
	end
	
	file:close()
	
end

function loadTable()
	
	local index = 1
	local jndex = 1
	
	local file = assert(io.open("table.txt", "r"))		
		--for s in file:gmatch("%S+") do
		for line in file:lines() do	
			for s in line:gmatch("%S+") do			
				QTable[index][jndex] = tonumber(s)
				jndex = jndex + 1
				if jndex > 7 then
					jndex = 1
					index = index + 1
				end
			end
		end
		
	file:close()
	
end

InitQTable() --Initializes Q table to 0
savestate.load(Filename)

form = forms.newform(300,300, "DonkAI Kong Form")
saveButton = forms.button(form, "Save", saveTable, 0, 0)
loadButton = forms.button(form, "Load", loadTable, 0, 20)

while true do
	FreezeEnemyPositions() --Freeze enemies for midterm demo
	gui.text(150,190,"Epsilon calculation: " .. exploreOrExploit,white)
	--gui.text(150,210,"Action chosen: " .. Buttons[actionTaken],white)

	--Logic for the start of a timeset
	if frameCounter == 0 then 
		marioOldX = GetMarioX() --Get initial x
		marioOldY = GetMarioY() --Get initial y
		local stateIndex = GetIndexFromPosition(marioOldX, marioOldY) --Get the index of the state given initial x and y
		actionTaken = TakeAction(stateIndex) --Takes that and gets the best action

	end
	--End of timestep; should have completed the action by now
	if frameCounter == fixedFramesPerAction - 1 then
		ClearButtons() --Unset all buttons

		--Set q value
		local marioNewX = GetMarioX() --Get new x
		local marioNewY = GetMarioY() --Get  new y
		local stateIndex = GetIndexFromPosition(marioOldX, marioOldY) --Get index of the initial state
		local newStateIndex = GetIndexFromPosition(marioNewX, marioNewY) --Get index of the new state
	
		--Q Formula
		--Note: Reward function is currently 255 - new Y position, just to make it try to maximize the reward rather than minimize
		QTable[stateIndex][actionTaken] = QTable[stateIndex][actionTaken] + learningRate * ((255 - marioNewY) + gamma * (QTable[newStateIndex][GetBestAction(newStateIndex)] - QTable[stateIndex][actionTaken]))
		

	end

	joypad.set(controller) --Sets controller every frame

	--Reset state if dead or user defined timeout
	if GetMarioStatus() == 0x00FF or timeoutFrameCounter == timeoutFrames - 1 then
		local badState = GetIndexFromPosition(GetMarioX(), GetMarioY())
		QTable[badState][actionTaken] = -99
		savestate.load(Filename)
	end

	--Update frame count
	frameCounter = (frameCounter + 1) % fixedFramesPerAction
	--Update timeout counter if Mario hasn't moved
	if marioOldX == GetMarioX() and marioOldY == GetMarioY() then
		timeoutFrameCounter = (timeoutFrameCounter + 1) % timeoutFrames
	else --If he moves, reset that counter
		timeoutFrameCounter = 0
	end

	emu.frameadvance()
end