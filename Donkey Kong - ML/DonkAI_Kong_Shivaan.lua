backgroundColor = 0x80808080
borderColor = 0xFF000000
white= 0xFFFFFFFF
red = 0xFFFF0000
BoxRadius = 6
boxStartX = 10
boxStartY = 30
boxSide = 70
boxScale = 255/boxSide
marioSpriteSize = 16
enemySpriteSize = 17

Filename = "classicsTestState.state"
ButtonNames = {
	"A",
	"B",
	"Up",
	"Down",
	"Left",
	"Right",
}

Level1={}
Level1[0]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[1]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[2]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[3]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[4]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[5]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[6]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[7]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[8]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[9]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[10] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[11] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[12] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[13] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[14] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,0,0,0,0}
Level1[15] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[18] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[19] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[20] = {0,0,0,0,0,0,0,1,1,1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
Level1[21] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0}
Level1[22] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[23] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[24] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0}
Level1[25] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0}
Level1[26] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,0,0,0,0,0,0}
Level1[27] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[28] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[29] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[30] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[31] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[32] = {0,0,0,0,0,0,0,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
Level1[33] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[34] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[35] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[36] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[37] = {0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[38] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,0,0,0,0,0}
Level1[39] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[41] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[42] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[43] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0}
Level1[44] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
Level1[45] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[46] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[47] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[48] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Level1[49] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}

EnemyXPositions = {} 
EnemyYPositions = {} 

savestate.load(Filename)

function GetMarioPosition()
	--Use readbyte because we only read the lower half for position values
	marioX = memory.read_u8(0x0046)
	marioY = memory.read_u8(0x0047)
end

function GetEnemyPositions()
	local xMem = 0x0213
	local yMem = 0x0214
	for i=0, 11 do
		--Use readbyte because we only read the lower half for position values
		EnemyXPositions[i] = memory.readbyte(xMem)
		EnemyYPositions[i] = memory.readbyte(yMem)
		xMem = xMem + 0x0010
		yMem = yMem + 0x0010
	end
end

--General purpose function to draw something in the minimap
function DrawPosInBox(worldX, worldY, spriteScale, col)
    local localX = worldX/boxScale + boxStartX
    local localY = worldY/boxScale + boxStartY
    gui.drawRectangle(localX, localY, spriteScale/boxScale, spriteScale/boxScale, borderColor, col)
end

function DrawMapInBox(worldX, worldY, col)
    local localX = worldX/boxScale + boxStartX
    local localY = worldY/boxScale + boxStartY
    gui.drawRectangle(localX, localY, 2, 2, borderColor2, col)
end


function DrawMinimap()
	--Background
	gui.drawRectangle(boxStartX, boxStartY, boxSide, boxSide, borderColor, backgroundColor)
	--Characters - Mario
	DrawPosInBox(marioX, marioY, marioSpriteSize, white)
	--Characters - Enemies
	for i=0, 11 do
        if EnemyXPositions[i] ~= 255 and EnemyYPositions[i] ~= 255 then
            DrawPosInBox(EnemyXPositions[i], EnemyYPositions[i], enemySpriteSize, red)
        end
	end
		--Hardcoded Map
	for i=0, 49 do
		for j=0, 49 do
			if Level1[i][j] == 1 then
				DrawMapInBox(j*5,i*5, pink)
			end
			if Level1[i][j] == 2 then
				DrawMapInBox(j*5,i*5, lightblue)
			end
        end
    end
end

function DrawFOV()
	local offset = 3
	--Around Mario
	gui.drawRectangle(marioX + offset - marioSpriteSize/2, marioY + offset - marioSpriteSize/2, marioSpriteSize, marioSpriteSize, borderColor, backgroundColor)
	--Bottom right
	gui.drawRectangle(marioX + offset - marioSpriteSize/2 - marioSpriteSize, marioY + offset - marioSpriteSize/2, marioSpriteSize, marioSpriteSize, borderColor, backgroundColor)
	--Bottom left
	gui.drawRectangle(marioX + offset - marioSpriteSize/2 + marioSpriteSize, marioY + offset - marioSpriteSize/2, marioSpriteSize, marioSpriteSize, borderColor, backgroundColor)
	--Top right
	gui.drawRectangle(marioX + offset - marioSpriteSize/2 - marioSpriteSize, marioY + offset - marioSpriteSize/2 - marioSpriteSize, marioSpriteSize, marioSpriteSize, borderColor, backgroundColor)
	--Top center
	gui.drawRectangle(marioX + offset - marioSpriteSize/2, marioY + offset - marioSpriteSize/2 - marioSpriteSize, marioSpriteSize, marioSpriteSize, borderColor, backgroundColor)
	--Top left
	gui.drawRectangle(marioX + offset - marioSpriteSize/2 + marioSpriteSize, marioY + offset - marioSpriteSize/2 - marioSpriteSize, marioSpriteSize, marioSpriteSize, borderColor, backgroundColor)
end

function DrawEnemiesInFOV()
	local offset = 3
	local marioCenterX = marioX + offset
	local marioCenterY = marioY + offset

	local maxToTheRight = marioCenterX + marioSpriteSize/2 + marioSpriteSize
	local maxToTheLeft = marioCenterX - marioSpriteSize/2 - marioSpriteSize
	local maxToTheTop = marioCenterY - marioSpriteSize/2 - marioSpriteSize
	local maxToTheBottom = marioCenterY - marioSpriteSize/2

	for i = 0, 11 do
		if EnemyXPositions[i] ~= 255 and EnemyYPositions[i] ~= 255 then
			if EnemyXPositions[i] + offset < maxToTheRight and EnemyXPositions[i] - offset > maxToTheLeft then
				gui.drawRectangle(EnemyXPositions[i], EnemyYPositions[i], 16, 16, borderColor, red)
			end
		end
	end
end

marioTable = {}
for i = 1, 255 do
	marioTable[i] = {}
	for j = 1, 255 do
		marioTable[i][j] = 0
	end
end

function saveTable()
	
	local file = assert(io.open("table.txt", "w"))
	
	for i = 1, 255 do
		for j = 1, 255 do
			file:write(marioTable[i][j], " ")
		end
	end
	
	file:close()
	
end

function loadTable()
	
	local index = 1
	local jndex = 1
	
	local file = assert(io.open("table.txt", "r"))		
		for line in file:lines() do	
			for s in line:gmatch("%S+") do			
				marioTable[index][jndex] = tonumber(s)--+1
				jndex = jndex + 1
				if jndex > 255 then
					jndex = 1
					index = index + 1
				end
			end
		end
		
	file:close()
	
	--print(marioTable[1][1])		
	--print(marioTable[255][255])	
	
end

--Main loop
while true do
	GetMarioPosition()
	GetEnemyPositions()
	--DrawMinimap()
	DrawFOV()
	DrawEnemiesInFOV()
	emu.frameadvance()
end